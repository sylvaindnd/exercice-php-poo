<?php 

class ShoppingCart{
    public function __construct()
    {
        $this->items = array();

        if(!isset($GLOBALS['cartIds'])){
            $GLOBALS['cartIds'] = 0;            
        }else{
            $GLOBALS['cartIds'] += 1;
        }

        $this->id = $GLOBALS['cartIds'];        
    }

    public function addItem($item){
        $totalWeight = $item->getWeight() + $this->weightCountInt();
        
        if($totalWeight>10000){
            throw new ErrorException('The cart is too heavy, impossible to add new items (max 10kg)');
            return;
        }

        $this->items[] = $item;
    }

    public function removeItem($item){
        $find = false;
        $find_index = 0;

        for($i=0; $i<=count($this->items)-1; $i++){
            if($this->items[$i]->__toString() === $item->__toString()){
                $find_index = $i;
                $find = true;
            }
        }

        array_splice($this->items, $find_index, 1);

        return $find;
    }

    public function weightCount(){
        $totalWeight = $this->weightCountInt();        

        return number_format((float)($totalWeight/1000), 3, '.', '') . ' kg';
    }

    public function weightCountInt(){
        $totalWeight = 0;

        foreach ($this->items as $item) {
            $totalWeight += $item->getWeight();
        }

        return $totalWeight;
    }

    public function itemsCount(){
        return count($this->items);
    }

    public function getTotalPrice(){
        $totalPrice = $this->getTotalPriceInt();     

        return number_format((float)($totalPrice/100), 2, '.', '') . ' €';
    }

    public function getTotalPriceInt(){
        $totalPrice = 0;

        foreach ($this->items as $item) {
            $totalPrice += $item->getPrice();
        }

        return $totalPrice;
    }

    public function getId(){
        return $this->id;
    }

    public function getItems(){
        return $this->items;
    }

    public function __toString()
    {
        return 'Cart n°' . $this->getId() . ': ' . $this->getTotalPrice() . ', ' . $this->weightCount();
    }
}