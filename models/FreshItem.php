<?php
require_once 'Items.php';

class FreshItem extends Item{
    public function __construct($name, $price, $weight, $bestBeforeDate)
    {
        $this->name = $name;
        $this->price = $price;
        $this->weight = $weight;
        $this->bestBeforeDate = $bestBeforeDate;
    }

    public function getBestBeforeDate(){
        return $this->bestBeforeDate;
    }

    public function __toString()
    {
        return $this->name . ': ' . $this->getWeight() . 'g, '. $this->priceToEuro() . ', expire at ' . $this->getBestBeforeDate();
    }
}