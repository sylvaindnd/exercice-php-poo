<?php

class Item{
    public function __construct($name, $price, $weight)
    {
        $this->name = $name;
        $this->price = $price;
        $this->weight = $weight;
    }

    public function getWeight(){
        return $this->weight;
    }

    public function getName(){
        return $this->name;
    }

    public function getPrice(){
        return $this->price;
    }

    public function priceToEuro(){
        return number_format((float)($this->price/100), 2, '.', '') . ' €';
    }

    public function __toString()
    {
        return $this->name . ': ' . $this->getWeight() . 'g, '. $this->priceToEuro();
    }
}