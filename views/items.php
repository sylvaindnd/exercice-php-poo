<div id="items" class="content">

<div class="item_title">
<h2>Find all items here</h2>
</div>

<?php

    $items = array();
    
    if(array_key_exists('items', $data)){
        $items = $data['items'];
    }

    foreach ($items as $key => $item) {
        ?>
            <div class="item">
                <span class="title"><?= $item->getName() ?></span>
                <span class="name"><i>name</i> <?= var_dump($item->getName()) ?></span>
                <span class="weight"><i>weight (g)</i> <?= var_dump($item->getWeight()) ?></span>
                <span class="price"><i>price (c)</i> <?= var_dump($item->getPrice()) ?></span>
                <span class="full"><i>toString()</i> <?= var_dump($item->__toString()) ?></span>
            </div>
        <?php
    }

?>

</div>
