<div id="carts" class="content">

<div class="carts_title">
<h2>Find your carts</h2>
</div>

<?php

    $carts = array();
    
    if(array_key_exists('carts', $data)){
        $carts = $data['carts'];
    }

    foreach ($carts as $key => $cart) {
        ?>
            <div class="cart">
                <span class="title">Cart n°<?= $cart->getId() ?> ( <i><?= $cart->getTotalPrice() ?></i>, <i><?= $cart->weightCount() ?></i> )</span>
                <span class="name"><i>id</i> <?= var_dump($cart->getId()) ?></span>
                <span class="items"><i>items</i> <?= var_dump($cart->itemsCount()) ?></span>
                <span class="weight"><i>weight (g)</i> <?= var_dump($cart->weightCountInt()) ?></span>
                <span class="price"><i>price (c)</i> <?= var_dump($cart->getTotalPriceInt()) ?></span>
                <span class="full"><i>toString()</i> <?= var_dump($cart->__toString()) ?></span>
                <div class="cart_items">
                    <span>Items :</span>
                    <ul>
                        <?php
                        foreach ($cart->getItems() as $key => $item){
                            echo '<li>' . $item->__toString(). '</li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>
        <?php
    }

?>

</div>
