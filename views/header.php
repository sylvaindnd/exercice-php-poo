<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/templates/css/style.css">
    <title>Supermarket</title>
</head>

<body>

    <nav>
        <ul>
            <li>
                <a href="/items/">
                    <span class="material-symbols-outlined">category</span>   
                    Items
                </a>

                <?php

                if($urls[0] === ''){
                    echo '<span class="special_link material-symbols-outlined">arrow_upward</span>';
                }

                ?>
            </li>

            <li>
                <a href="/freshitems/">
                    <span class="material-symbols-outlined">ac_unit</span>    
                    Fresh items
                </a>
            </li>

            <li>
                <a href="/carts/">
                    <span class="material-symbols-outlined">shopping_cart</span>
                    Carts
                </a>
            </li>         

            <li>
                <a href="/tickets/">
                    <span class="material-symbols-outlined">receipt_long</span> 
                    Tickets
                </a>
            </li>
        </ul>
    </nav>


    <?php 

    if($urls[0] === ''){
        echo '<div class="special_image"><img src="/images/shop.jpg"></div>';
    }

    ?>