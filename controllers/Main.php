<?php 

function generateRandomString($length = 10) {
    return substr(str_shuffle(str_repeat($x='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}

function fakeItems($length=10){
    require_once 'models/Items.php';

    $fakeItemsList = array();

    for($i=0; $i<$length; $i++){
        $name = generateRandomString(random_int(4, 15));
        $price = random_int(10, 1000);
        $weight = random_int(10, 500);

        $fakeItem = new Item($name, $price, $weight);
        $fakeItemsList[] = $fakeItem;
    }    
    
    return $fakeItemsList;
}

class Main{
    public function __construct()
    {
        $this->render();
    }

    public function urls(){
        $paramters = $_SERVER['REQUEST_URI'];
        if(substr($paramters, -1) === '/'){
            $paramters = substr_replace($paramters, '', -1);
        }
        if(substr($paramters, 0, 1) === '/'){
            $paramters = substr_replace($paramters, '', 0, 1);
        }
        return explode('/', $paramters);
    }

    public function render(){
        $urls = $this->urls();
        $data = array();

        require_once 'views/header.php';

        if($urls[0] === 'items'){
            require_once 'models/Items.php';

            $data['items'] = array();
            $data['items']['corn_flakes'] = new Item('corn flask', 500, 700);
            $data['items']['chewing_gum'] = new Item('chewing gum', 403, 30);

            foreach(fakeItems(9) as $item){
                $data['items'][$item->getName()] = $item;
            }
            
            require_once 'views/items.php';
        }

        if($urls[0] === 'freshitems'){
            require_once 'models/FreshItem.php';

            $data['items'] = array();
            $data['items']['fish'] = new FreshItem('salmon', 1295, 1200, '2022-12-13');
            $data['items']['steack'] = new FreshItem('chicken steak', 989, 825, '2022-11-19');
            
            require_once 'views/items.php';
        }

        if($urls[0] === 'carts'){
            require_once 'models/Items.php';
            require_once 'models/ShoppingCart.php';

            $data['carts'] = array();

            $cart = new ShoppingCart();
            foreach(fakeItems(10) as $item){
                $cart->addItem($item);
            }
            $data['carts'][] = $cart;

            $cart = new ShoppingCart();
            foreach(fakeItems(3) as $item){
                $cart->addItem($item);
            }
            $data['carts'][] = $cart;

            $cart = new ShoppingCart();
            foreach(fakeItems(5) as $item){
                $cart->addItem($item);
            }
            $data['carts'][] = $cart;

            require_once 'views/carts.php';
        }

        if($urls[0] === 'tickets'){
            require_once 'views/tickets.php';
        }

        require_once 'views/footer.php';
    }
}
